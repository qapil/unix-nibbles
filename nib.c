#include "nib.h"

/**********/

const char wallch = '+';
const char nibinitmsg[] = "Nibbles game v1";
const char player_char[] = "o*+x$%";

/**********/

int play_cnt;
int collision, targetout;
char board[BOARD_HEIGHT][BOARD_WIDTH];
struct target target;
struct player player[MAXPLAYERS];
struct POINT heads[MAXPLAYERS];

/**********/

void set_bit(unsigned char *p, int n)
{
    *(p+n/8) |= 1 << (n%8);
}
int get_bit(unsigned char *p, int n)
{
    return *(p+n/8) & (1 << (n%8));
}

void initplayers(int n)
{
    int i;
    
    for (i=0; i < n; i ++) {
	player[i].score = 0;
	player[i].lives = LIVES;
	player[i].collision = 0;
	player[i].length = 0;
	player[i].append = INITNIBBLESIZE;
#ifdef NIBBLES_CLIENT
	player[i].color = COLOR_PAIR(i+1);
	if (i == 1 || i == 2 || i == 3)
	    player[i].color |= A_BOLD;
#else
	player[i].color = 0;
#endif
	player[i].ch = player_char[i];
    }
}

int alive(void) 
{
    int i;

    for (i=0; i < play_cnt; i ++) 
	if (player[i].lives > 0) return 1;
    return 0;
}

void check(struct POINT pt)
{
    int i;
    
    for (i=0; i < play_cnt; i ++)
	if (player[i].lives > 0 && pt.x == heads[i].x && pt.y == heads[i].y)
	    player[i].collision = collision = 1;
}
void mvnibends(void)
{
    int i, tmp;
    
    for (i=0; i < play_cnt; i ++)
	if (player[i].lives > 0) {
	    tmp = player[i].length-1;
	    if (player[i].append > 0) {
		player[i].nibble[tmp+1] = player[i].nibble[tmp];
		player[i].length ++;
		player[i].append --;
		check(player[i].nibble[tmp+1]);
	    }
	    else {
#ifdef NIBBLES_CLIENT	    
		attrset(NORMAL_COLOR);
		mvaddch(player[i].nibble[tmp].y, player[i].nibble[tmp].x, ' ');
#endif
		player[i].nibble[tmp] = player[i].nibble[tmp-1];
	    }
	}
}
void mvnibbodies(void)
{
    int i, tmp;
    
    for (i=0; i < play_cnt; i ++)
	if (player[i].lives > 0)
	    for (tmp = player[i].length-2; tmp > 0; tmp --) {
		player[i].nibble[tmp] = player[i].nibble[tmp-1];
		check(player[i].nibble[tmp]);
	    }
}
void mvnibheads(void)
{
    int i, j;
    
    for (i=0; i < play_cnt; i ++)
	if (player[i].lives > 0) {
	    player[i].head.x = heads[i].x;
	    player[i].head.y = heads[i].y;
#ifdef NIBBLES_CLIENT	
	    attrset(player[i].color);
	    mvaddch(player[i].head.y, player[i].head.x, player[i].ch);
#endif
	    for (j=i+1; j < play_cnt; j ++)
		if (player[j].lives > 0 && 
		    heads[i].x == heads[j].x && heads[i].y == heads[j].y)
		    player[i].collision = player[j].collision = collision = 1;
	    if (board[heads[i].y][heads[i].x]) 
		player[i].collision = collision = 1;
	    if (heads[i].x == target.x && heads[i].y == target.y) {
		targetout = 1;
		player[i].score += target.num;
		player[i].append += target.num*3;
	    }
	}
}
