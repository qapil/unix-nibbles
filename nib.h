#ifndef _NIB_H
#define _NIB_H

#define MAXNIBBLESIZE	141
#define MAXPLAYERS	6
#define ADDRLEN		64
#define BOARD_WIDTH	80
#define BOARD_HEIGHT	23
#define LIVES		3
#define INITNIBBLESIZE	5

#define FRAME_COLOR	COLOR_PAIR(MAXPLAYERS+1)
#define NORMAL_COLOR	COLOR_PAIR(MAXPLAYERS+2)

/**********/

struct POINT {
    int x, y;
};

struct player {
    int score;
    int lives;
    int collision;
    int length;
    int append;
    int color;
    char ch;
    struct POINT nibble[MAXNIBBLESIZE];
};
#define head	nibble[0]

struct target {
    int num;
    int x, y;
};

#endif
