CC=gcc

# to compile on linux leave the following line uncommented, othervise comment it
LIBS=
# to compile on solaris uncomment following line and comment the above one
#LIBS=-lsocket -lnsl

all: nibserv nibbles

nibserv: nibserv.o
	$(CC) $^ $(LIBS) -o $@

nibbles: nibbles.o
	$(CC) $^ $(LIBS) -lcurses -o $@

nibbserv.o: nibserv.c nib.c nib.h
	$(CC) -c nibserv.c -o $@

nibbles.o: nibbles.c nib.c nib.h
	$(CC) -c nibbles.c -o $@

clean:
	rm -f nibserv.o nibbles.o

cleanall: clean
	rm -f nibserv nibbles
