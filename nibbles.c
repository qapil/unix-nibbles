#include <stdio.h>
#include <netdb.h>
#include <unistd.h>
#include <sys/time.h>
#include <sys/types.h>
#include <sys/param.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <curses.h>
#define NIBBLES_CLIENT
#include "nib.c"

#define DEBUG

/********************************/
/* directions 0 1 2 3 : N W S E */
/********************************/

int s;
char host[MAXHOSTNAMELEN+1];
char address[ADDRLEN+1];
int play_num, level;
int usecolors;

/**********/

void initcolors(void)
{
    int i;
    
    start_color();
    if (has_colors() && usecolors) {
	init_pair(1, COLOR_BLUE, COLOR_CYAN);
	init_pair(2, COLOR_YELLOW, COLOR_MAGENTA);
	init_pair(3, COLOR_YELLOW, COLOR_BLUE);
	init_pair(4, COLOR_WHITE, COLOR_GREEN);
	init_pair(5, COLOR_GREEN, COLOR_YELLOW);
	init_pair(6, COLOR_MAGENTA, COLOR_WHITE);
	init_pair(7, COLOR_BLACK, COLOR_WHITE);
    }
    else 
	for (i=1; i <= MAXPLAYERS+1; i ++)
	    init_pair(i, COLOR_BLACK, COLOR_WHITE);
    init_pair(8, COLOR_WHITE, COLOR_BLACK);
}

void drawscores(int n)
{
    int i;
    char str[10];
    
    attrset(NORMAL_COLOR);
    sprintf(str, "level %.3i", level);
    mvaddstr(BOARD_HEIGHT,  3, str);
    
    for (i=0; i < n; i ++) {
	attrset(player[i].color);
	sprintf(str, " %.5i / %i ", player[i].score, player[i].lives);
	mvaddstr(BOARD_HEIGHT, 14 + i*11, str);
    }
}

void setup_players()
{
    int i;
    unsigned char ch;
    
    for (i=0; i < play_cnt; i ++) {
	while (read(s, &ch, 1) == 0) ;
	player[i].head.x = ch;
	while (read(s, &ch, 1) == 0) ;
	player[i].head.y = ch;
	player[i].collision = 0;
	player[i].length = 1;
	player[i].append = INITNIBBLESIZE;
    }
    collision = 0;
    target.num = 0;
}
void drawboard(void)
{
    int i, j;
    
    for (i=0; i < BOARD_WIDTH; i ++)
	for (j=0; j < BOARD_HEIGHT; j ++) {
	    if (board[j][i] == 0) {
		attrset(NORMAL_COLOR);
		mvaddch(j, i, ' ');
	    }
	    else {
		attrset(FRAME_COLOR);
		mvaddch(j, i, wallch);
	    }
	}
}
void gettarget()
{
    unsigned char ch;
    
    while (read(s, &ch, 1) == 0) ;
    target.x = ch;
    while (read(s, &ch, 1) == 0) ;
    target.y = ch;
    target.num ++;
    targetout = 0;
}
void countdown(void)
{
    int i;
    unsigned char ch;
    
    drawboard();
    drawscores(play_cnt);
    attrset(NORMAL_COLOR);
    mvaddstr(BOARD_HEIGHT/2-1, BOARD_WIDTH/2-11, "your nibble:");
    mvaddstr(BOARD_HEIGHT/2, BOARD_WIDTH/2-4, "countdown");
    move(BOARD_HEIGHT/2-1, BOARD_WIDTH/2+2);
    attrset(player[play_num].color);
    for (i=0; i < 9; i ++) addch(player[play_num].ch);
    attrset(NORMAL_COLOR);
    
    for (i=3; i > 0; i --) {
	while (read(s, &ch, 1) == 0) ;
	mvaddch(BOARD_HEIGHT/2+1, BOARD_WIDTH/2, '0' + ch);
        refresh();
    }
}

void play(void)
{
    int cnt, tmp, i, j;
    unsigned char moves[MAXPLAYERS], ch;
    fd_set read_set;
    char str[3];
    unsigned char board_bits[(BOARD_WIDTH*BOARD_HEIGHT+7)/8];
    
    level = 0;

    while (alive()) {
	cnt = 0;
	while (cnt < (BOARD_WIDTH*BOARD_HEIGHT+7)/8) {
	    cnt += tmp = read(s, board_bits+cnt, (BOARD_WIDTH*BOARD_HEIGHT+7)/8-cnt);
	    if (tmp < 0) {
		endwin();
		perror("chyba pri ziskavani levelu");
		fprintf(stderr, "precetl jsem %i z %i\n", cnt, BOARD_WIDTH*BOARD_HEIGHT);
		close(s);
		exit(-1);
	    }
	}
	for (j=0; j < BOARD_HEIGHT; j ++)
	    for (i=0; i < BOARD_WIDTH; i ++)
		board[j][i] = get_bit(board_bits, j*BOARD_WIDTH+i);
		
	level ++;
	setup_players();
	countdown();
	drawboard();
	
	while (alive() && target.num < 9) {
	    drawscores(play_cnt);
	    gettarget();
	    attrset(NORMAL_COLOR);
	    mvaddch(target.y, target.x, '0'+target.num);	/* show target */
	    
	    while ( ! collision && ! targetout) {
		FD_ZERO(&read_set);
		FD_SET(s, &read_set);				/* network connection */
		select(s+1, &read_set, NULL, NULL, NULL);
		if (FD_ISSET(s, &read_set)) {
		    read(s, &ch, 1);
		    ch = (unsigned char) getch();
		    write(s, &ch, 1);
		}

		FD_ZERO(&read_set);
		FD_SET(s, &read_set);				/* network connection */
		select(s+1, &read_set, NULL, NULL, NULL);
		if (FD_ISSET(s, &read_set)) {			/* from server */
		    cnt = 0;
		    while (cnt < play_cnt) {
			cnt += tmp = read(s, &(moves)+cnt, play_cnt-cnt);
			if (tmp == -1) {
			    endwin();
			    perror("chyba");
			    close(s);
			    exit(-1);
			}
		    }
		    for (i=0; i < play_cnt; i ++)
			if (player[i].lives) {
			    heads[i] = player[i].head;
			    switch (moves[i]) {
				case 0:				/* move up */
				    heads[i].y --; break;
				case 1:				/* move left */
				    heads[i].x --; break;
				case 2:				/* move down */
				    heads[i].y ++; break;
				case 3:				/* move right */
				    heads[i].x ++; break;
			    }
			}
		    mvnibends();
		    mvnibbodies();
		    mvnibheads();
		    move(BOARD_HEIGHT, 0);
		    refresh();
		}
	    }
	    if (collision) {
		for (i=0; i < play_cnt; i ++)
		    if (player[i].collision) player[i].lives --;
		if (alive()) {
		    setup_players();
		    countdown();
		    drawboard();
		}
	    } 
	}
    }
}

int main(int argc, char *argv[], char *argp[])
{

    struct hostent *he;
    struct sockaddr_in sin;
    char str[10];
    int port, i, cnt;
    unsigned char ch;
    
    printf("\n\tNibbles game client version 0.01\n");
    printf("\twritten by Martin Kvapil\n\n\n");

    printf("Do you want to use colors. (y/n)\n");
    do {
	printf("\tuse colors=");
	i = getchar();
	if (i == 'y' || i == 'Y') usecolors = 1;
	else if (i == 'n' || i == 'n') usecolors = 0;
	else usecolors = -1;
	while (getchar() != '\n') ;
    } while (usecolors < 0);
    
    printf("Enter the addres to Nibbles game server.\n");
    printf("\tserver=");
    if (fgets(host, MAXHOSTNAMELEN, stdin) == NULL) {
	perror("error");
	exit(-1);
    }
    sscanf(host, "%s\n", host);

    if ((he = gethostbyname(host)) == 0) {
	perror("error");
	exit(-1);
    }
    s = socket(PF_INET, SOCK_STREAM, 0);
    if (s < 0) {
	perror("error");
	exit(-1);
    }
    memset(&sin, 0, sizeof(sin));
    sin.sin_family = AF_INET;
    memcpy(&sin.sin_addr, he->h_addr, he->h_length);
    
    printf("Enter the port to connect to Nibbles game server\n");
    do {
	printf("\tport=");
	fgets(str, 10, stdin);
    } while ((port=atoi(str)) <= 0);

    sin.sin_port = port;
    
    if (connect(s, (struct sockaddr *) &sin, sizeof(sin)) != 0) {
	perror("error");
	exit(-1);
    }
    gethostname(address, ADDRLEN);
    
    printf("\n\tNibbles game client version 0.01\n");
    printf("\twritten by Martin Kvapil\n\n\n");
    
    COLS = LINES = 0;
    if (initscr() == NULL) {
	printf("initscr failed: TERM is unset or unknown terminal type.\n");
	exit(-1);
    }
    if (COLS < BOARD_WIDTH || LINES < BOARD_HEIGHT+1) {
	endwin();
	fprintf(stderr, "You need at least %ix%i term screen size\n", BOARD_WIDTH, BOARD_HEIGHT+1);
	fprintf(stderr, "ans you have only %ix%i\n", COLS, LINES);
	exit(-1);
    }
    clear();
    refresh();
    cbreak();
    noecho();
    nodelay(stdscr, TRUE);
    intrflush(stdscr, FALSE);
    initcolors();

    write(s, nibinitmsg, strlen(nibinitmsg));
    write(s, address, ADDRLEN);
    while (read(s, &ch, 1) == 0 ) ;
    play_cnt = ch;
    while (read(s, &ch, 1) == 0 ) ;
    play_num = ch;

    attrset(NORMAL_COLOR);
    for (i=0; i < play_cnt; i ++) {
	cnt = 0;
	while ((cnt += read(s, address+cnt, ADDRLEN-cnt)) < ADDRLEN) ;
	if (i == play_num) mvaddstr(i+1, 1, "* ");
	else mvaddstr(i+1, 1,"  ");
	addstr(address);
	refresh();
    }
        
    clear();
    initplayers(play_cnt);
    drawscores(play_cnt);
        
    play();
    
    close(s);
    endwin();

    exit(0);
}
