#include <stdio.h>
#include <stdlib.h>
#include <netdb.h>
#include <sys/time.h>
#include <sys/param.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <signal.h>
#include "nib.c"

#define MAXPORT		65536
#define MAXDELAYTIME	500000

/***********/

const char leveldir[] = "./levels";
const struct POINT playerpos[MAXPLAYERS] = {
    { 15, 11 },
    { 65, 11 },
    { 52,  3 },
    { 28, 19 },
    { 28,  3 },
    { 52, 19 }
};
const unsigned char playermove[MAXPLAYERS] = { 0, 2, 3, 1, 3, 1 };

int s, play_cnt, allsocks;
char host[MAXHOSTNAMELEN+1];
int sock[MAXPLAYERS];
int delaytime, level, maxlevel;
unsigned char moves[MAXPLAYERS];

/***********/

int countlevels(void)
{
    FILE *fp;
    char fname[80];
    int i = 1;
    
    sprintf(fname, "%s/level%.2i", leveldir, i);
    while (fp = fopen(fname, "r")) {
	close(fp);
	i ++;
	sprintf(fname, "%s/level%.2i", leveldir, i);
    }
    return i-1;
}
void terminate(int arg)
{
    int i;
    
    printf("Nibbles game server is getting down\n");
    for (i=0; i < play_cnt; i ++)
	close(sock[i]);
    exit(0);
}

int testport(int port, struct sockaddr_in *sin)
{
    sin->sin_port = port;
    return !bind(s, (struct sockaddr *) sin, sizeof(*sin));
}

int freeport(struct sockaddr_in *sin)
{
    int port = sin->sin_port = 1024;
    
    while (port < MAXPORT && bind(s, (struct sockaddr *) sin, sizeof(*sin)) != 0) {
	port ++;
	sin->sin_port = port;
    }
    return port == MAXPORT ? -1 : port;
}

void sendboard(void)
{
    FILE *fp;
    char fname[80];
    int i, j, tmp;
    unsigned char board_bits[(BOARD_WIDTH*BOARD_HEIGHT+7)/8];
    
    if (level > maxlevel) {
	level = 1;
	delaytime *= 0.9;
    }
    
    sprintf(fname, "%s/level%.2i", leveldir, level);
    if ((fp = fopen(fname, "r")) == NULL) {
	perror("error");
	exit(-2);
    }
    if ((i=fread(board, 1, BOARD_WIDTH*BOARD_HEIGHT, fp)) < BOARD_WIDTH*BOARD_HEIGHT) {
	perror("error");
	exit(-2);
    }
    fclose(fp);

    memset(board_bits, 0, (BOARD_WIDTH*BOARD_HEIGHT+7)/8);
    for (j=0; j < BOARD_HEIGHT; j ++)
	for (i=0; i < BOARD_WIDTH; i ++)
	    if (board[j][i] == '#') {
		set_bit(board_bits, j*BOARD_WIDTH+i);
		board[j][i] = 1;
	    }
	    else board[j][i] = 0;

    for (i=0; i < play_cnt; i ++) {
	j = 0;
	while (j < (BOARD_WIDTH*BOARD_HEIGHT+7)/8)
	    j += write(sock[i], board_bits+j, (BOARD_WIDTH*BOARD_HEIGHT+7)/8-j);
    }
}
void sendplayers(void)
{
    int i, j;
    unsigned char ch;
    
    for (i=0; i < play_cnt; i ++) {
	player[i].head.x = playerpos[i].x;
	player[i].head.y = playerpos[i].y;
	moves[i] = playermove[i];
		
	player[i].collision = 0;
	player[i].length = 1;
	player[i].append = INITNIBBLESIZE;
    }
    collision = 0;
    target.num = 0;
    
    for (i=0; i < play_cnt; i ++)
	for (j=0; j < play_cnt; j ++) {
	    ch = (unsigned char) player[j].head.x;
	    write(sock[i], &ch, 1);
	    ch = (unsigned char) player[j].head.y;
	    write(sock[i], &ch, 1);
	}
}
int checktarget()
{
    int i, j;
    
    if (board[target.y][target.x]) return 1;
    for (i=0; i < play_cnt; i ++)
	for (j=0; j < player[i].length; j ++)
	    if (player[i].nibble[j].x == target.x &&
		player[i].nibble[j].y == target.y)
		return 1;
    return 0;
}
void newtarget()
{
    do {
	target.x = 1 + rand()%(BOARD_WIDTH-2);
	target.y = 1 + rand()%(BOARD_HEIGHT-2);
    } while (checktarget());
}
void sendtarget(void)
{
    int i;
    unsigned char ch;
    
    newtarget();
    for (i=0; i < play_cnt; i ++) {
	ch = (unsigned char) target.x;
	write(sock[i], &ch, 1);
	ch = (unsigned char) target.y;
	write(sock[i], &ch, 1);
    }
    target.num ++;
    targetout = 0;
}

void play(void)
{
    int i, tmp;
    fd_set read_set;
    struct timeval nulltime;
    unsigned char ch;
    
    level = tmp = 0;

    while (alive()) {
	level ++;
	sendboard();
	sendplayers();
	for (ch=3; ch > 0; ch --) {			/* countdown */
	    for (i=0; i < play_cnt; i ++)
		write(sock[i], &ch, 1);
	    sleep(1);
	}
	
	while (alive() && target.num < 9) {
	    sendtarget();
	    while (! collision && ! targetout) {
		usleep(delaytime);

		for (i=0; i < play_cnt; i ++) {		/* read moves */
		    ch = 0; write(sock[i], &ch, 1);
		    FD_ZERO(&read_set);
		    FD_SET(sock[i], &read_set);
		    if (select(sock[i]+1, &read_set, NULL, NULL, NULL) == -1) {
			perror("chyba - select()");
			for(i=0; i <  play_cnt; i ++) close(sock[i]);
			exit(-1);
		    }
		    if (FD_ISSET(sock[i], &read_set)) {
			read(sock[i], &ch, 1) ;
			if (ch == ',') 
			    if (moves[i] == 3) moves[i] = 0;
			    else moves[i] += 1;
			if (ch == '.') 
			    if (moves[i] == 0) moves[i] = 3;
			    else moves[i] -= 1;	    
		        if (moves[i] > 3) moves[i] -= 4;
		    }
		}
	    
		for (i=0; i < play_cnt; i ++) {		/* send moves */
		    write(sock[i], moves, play_cnt);
		    if (player[i].lives) {
			heads[i] = player[i].head;
			switch (moves[i]) {
			    case 0:				/* move up */
				heads[i].y --; break;
			    case 1:				/* move left */
				heads[i].x --; break;
			    case 2:				/* move down */
				heads[i].y ++; break;
			    case 3:				/* move right */
				heads[i].x ++; break;
			}
		    }
		}
		mvnibends();
		mvnibbodies();
		mvnibheads();
	    }
	    if (collision) {
		for (i=0; i < play_cnt; i ++)
		    if (player[i].collision) player[i].lives--;
		if (alive()) {
		    sendplayers();
		    for (ch=3; ch > 0; ch --) {			/* countdown */
			for (i=0; i < play_cnt; i ++)
			    write(sock[i], &ch, 1);
			sleep(1);
		    }
		}
	    }
	}
    }
}

int main(int argc, char *argv[])
{
    struct hostent *he;
    struct sockaddr_in sin;
    char str[64];
    char address[ADDRLEN+1][MAXPLAYERS];
    int port, i, sin_size, cnt, j;
    unsigned char ch;
    
    printf("\n\tNibbles game server version 0.01\n");
    printf("\twritten by Martin Kvapil\n\n\n");
    
    if ((maxlevel = countlevels()) == 0) {
	fprintf(stderr, "I'm sorry bud I can't find any level files in directory %s\n", leveldir);
	exit(-1);
    }
    printf("Found %i level files\n", maxlevel);
    if (gethostname(host, MAXHOSTNAMELEN)) {
	perror("error");
	exit(-1);
    }
    printf("hostname: %s\n", host);
    if ((he = gethostbyname(host)) == 0) {
	perror("error");
	exit(-1);
    }
    s = socket(PF_INET, SOCK_STREAM, 0);
    if (s < 0) {
	perror("error");
	exit(-1);
    }
    memset(&sin, 0, sizeof(sin));
    sin.sin_family = AF_INET;
    memcpy(&sin.sin_addr, he->h_addr, he->h_length);
    
    printf("Enter the port to connect with Nibbles clients. If you don't know which port is\n"
	   "free enter -1 and the first free port will be used.\n");
    do {
	printf("\tport=");
	fgets(str, 10, stdin);
    } while ((port = atoi(str)) == 0 || (port != -1 && !testport(port, &sin)));

    if (port == -1) port = freeport(&sin);
    if (port == -1) {
	fprintf(stderr, "I'm sorry, but you have no free ports to use with Nibbles game server.\n");
	exit(-1);
    }
    printf("using port: %i\n", port);
    
    printf("How many players may I wait for? (maximum is 6)\n");
    do {
	printf("\tplayers=");
	fgets(str, 10, stdin);
    } while ((play_cnt = atoi(str)) <= 0);
    if (play_cnt >= MAXPLAYERS) play_cnt = MAXPLAYERS;
    
    printf("enter the speed factor (1 - slow ... 9 - fast )\n");
    do {
	printf("\tspeed=");
	fgets(str, 10, stdin);
    } while ((delaytime = atoi(str)) < 1 || delaytime > 9);
    delaytime = MAXDELAYTIME / delaytime;

/****************/

    printf("I'm waiting to connect with %i players\n", play_cnt);    
    
    if (fork() != 0) {		// go to background
	exit(0);
    }

    if (listen(s, play_cnt)) {
	perror("error");
	exit(-1);
    }

    allsocks = 0;
    for (i=0; i < play_cnt; i ++)
    {
        sin_size = sizeof(sin);
        if ((sock[i] = accept(s, (struct sockaddr *) &sin, &sin_size)) <= 0) {
	    perror("error");
	    exit(-1);
	}
	if (allsocks <= sock[i]) allsocks = sock[i]+1;
	j = strlen(nibinitmsg);
	cnt = 0;
	while ((cnt += read(sock[i], str+cnt, j-cnt)) < j) ;
	if (strncmp(str, nibinitmsg, j)) i --;
	else {
	    cnt = 0;
	    while ((cnt += read(sock[i], address[i]+cnt, ADDRLEN-cnt)) < ADDRLEN) ;
	    ch = (unsigned char) play_cnt;
	    write(sock[i], &ch, 1);
	    ch = (unsigned char) i;
	    write(sock[i], &ch, 1);
	    for (j=0; j <= i; j ++)
		write(sock[i], address[j], ADDRLEN);
	    for (j=0; j < i; j ++)
		write(sock[j], address[i], ADDRLEN);
	}
    }
    close(s);    

    signal(SIGTERM, terminate);
    
    initplayers(play_cnt);
    play();
    
    terminate(0);
}
